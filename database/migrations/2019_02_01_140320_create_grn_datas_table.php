<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrnDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grn_datas', function (Blueprint $table) {
            // 'Item_Id','Qty','Available_Qty','Price','TotalPrice','Store_Id','Grn_Id','Barcode','Item_Price,'Selling_Price
            $table->increments('id');
            $table->integer('Item_Id')->index('fk_item_idx')->unsigned();
            $table->integer('Qty');
            $table->integer('Available_Qty');
            $table->float('Item_Price',10,0);
            $table->float('TotalPrice',10,0);
            $table->float('Selling_Price',10,0);
            $table->float('Item_Price',10,0);
            $table->integer('Store_id')->index('fk_store_idx')->unsigned();
            $table->integer('Grn_Id')->index('fk_grn_idx')->unsigned();
            $table->string('Barcode',45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grn_datas');
    }
}
