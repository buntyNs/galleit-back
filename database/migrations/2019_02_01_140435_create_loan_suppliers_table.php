<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_suppliers', function (Blueprint $table) {
            // 'Supplier_Id','Grn_Id','Amount','Due_Date','Status'
            $table->increments('id');
            $table->integer('Supplier_Id')->index('fk_Supplier_idx')->unsigned();
            $table->integer('Grn_Id')->index('fk_Grn_idx')->unsigned();
            $table->float('Amount',10,0);
            $table->dateTime('Due_Date');
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_suppliers');
    }
}
