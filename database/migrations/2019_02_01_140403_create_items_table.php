<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            // 'Name','Type_Id','Barcode','Selling_Price','Qty','Status'
            $table->increments('id');
            $table->string('Name',45);
            $table->string('Units',50)->nullable();
            $table->integer('Type_Id')->index('fk_type_idx')->unsigned();
            $table->string('Barcode',45);
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
