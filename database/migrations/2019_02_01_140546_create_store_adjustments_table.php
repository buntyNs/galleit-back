<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_adjustments', function (Blueprint $table) {
            // 'User_id','Store_Id','Item_Id','Qty'
            $table->increments('id');
            $table->integer('User_Id')->index('fk_User_idx')->unsigned();
            $table->integer('Store_Id')->index('fk_Store_idx')->unsigned();
            $table->integer('Item_Id')->index('fk_Item_idx')->unsigned();
            $table->integer('Old_Qty');
            $table->integer('New_Qty');
            $table->float('Old_Selling_Price',10,0);
            $table->float('New_Selling_Price',10,0);
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_adjustments');
    }
}
