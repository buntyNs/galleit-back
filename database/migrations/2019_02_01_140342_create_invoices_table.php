<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            // 'Discount','Total_Price','User_Id','Branch_Id','PaymentType_Id','Customer_Id','Status'
            $table->increments('id');
            $table->string('Invoice_No',50)->nullable();
            $table->float('Discount',10,0);
            $table->float('Total_Price',10,0);
            $table->integer('User_Id')->index('fk_User_idx')->unsigned();
            $table->integer('Branch_Id')->index('fk_Branch_idx')->unsigned();
            $table->integer('PaymentType_Id')->index('fk_PaymentType_idx')->unsigned();
            $table->integer('Customer_Id')->index('fk_Customer_idx')->unsigned();
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
