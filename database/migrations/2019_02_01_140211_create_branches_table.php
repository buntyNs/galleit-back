<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name',45)->nullable();
            $table->string('Address',45)->nullable();
            $table->string('Email',45)->nullable()->unique('Email');
            $table->integer('Contact')->nullable();
            $table->integer('Mid')->index('fk_user_idx')->unsigned();
            $table->integer('DefaultStore_Id')->index('fk_store_idx')->unsigned();
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
