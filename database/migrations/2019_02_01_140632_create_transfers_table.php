<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            // 'User_Id','From_Id','To_Id','Status'
            $table->increments('id');
            $table->integer('User_Id')->index('fk_user_idx')->unsigned();
            $table->integer('From_Id')->index('fk_Store_Form_idx')->unsigned();
            $table->integer('To_Id')->index('fk_store_To_idx')->unsigned();
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
