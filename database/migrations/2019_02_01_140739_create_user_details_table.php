<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            // 'Address','Contact','User_Id','Branch_Id',''
            $table->increments('id');
            $table->string('Address', 200);
			$table->integer('Contact');
			$table->integer('User_Id')->index('fk_User_idx')->unsigned();
			$table->integer('Branch_Id')->index('fk_Branch_idx')->unsigned();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
