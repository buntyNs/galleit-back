<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_returns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Customer_Id')->index('fk_customer_idx')->unsigned();
            $table->integer('Bill_Id');
            $table->string('Reson',200);
            $table->integer('Grn_Id')->nullable()->index('fk_Grn_idx')->unsigned();
            $table->integer('Qty');
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_returns');
    }
}
