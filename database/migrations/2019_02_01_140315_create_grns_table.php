<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grns', function (Blueprint $table) {
            // 'InvoiceId', 'Grn_Total', 'Stock_Id','Status','User_Id','Payment_Type_Id','Payment_Due','Supplier_Id,'Discount,'Grn_No,'Grn_Value
            $table->increments('id');
            $table->String('InvoiceId');
            $table->float('Grn_Total', 10, 0);
            $table->string('Grn_No',15);
            $table->integer('Stock_Id')->index('fk_stock_idx')->unsigned();
            $table->integer('User_Id')->index('fk_user_idx')->unsigned();
            $table->integer('Payment_Type_Id')->index('fk_PaymentType_idx')->unsigned();
            $table->integer('Supplier_Id')->index('fk_Supplier_idx')->unsigned();
            $table->dateTime('Payment_Due');
            $table->integer('Status')->default(0);
            $table->float('Discount',10,0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grns');
    }
}
