<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            // 'Name','Cid','Contact','Email','Address','Status','IdNumber'
            $table->increments('id');
            $table->string('Name',15);
            $table->string('Email',45)->unique()->nullable();
            $table->integer('Cid')->index('fk_company_idx')->unsigned();
            $table->integer('Contact')->nullable();
            $table->string('Address',200)->nullable();
            $table->string('IdNumber',20)->unique();
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
