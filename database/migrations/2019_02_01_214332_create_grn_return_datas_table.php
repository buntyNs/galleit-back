<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrnReturnDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grn_return_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Note_Id')->index('fk_Note_idx')->unsigned();
            $table->integer('Item_Id')->index('fk_Item_Id')->unsigned();
            $table->float('Total',10,0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grn_return_datas');
    }
}
