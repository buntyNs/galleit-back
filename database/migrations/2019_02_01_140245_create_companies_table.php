<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {

            //['Name', 'Email' ,'Contact' , 'Address','Status'];
            $table->increments('id');
            $table->string('CName',45)->nullable();
            $table->string('Email',45)->unique();
            $table->integer('Contact')->nullable();
            $table->string('Address',200)->nullable();
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
