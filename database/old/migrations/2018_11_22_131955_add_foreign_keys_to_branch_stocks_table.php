<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBranchStocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('branch_stocks', function(Blueprint $table)
		{
			$table->foreign('Branch_Id', 'fk_BranchStock_Branch1')->references('Id')->on('branches')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Store_Id', 'fk_BranchStock_Store1')->references('Id')->on('stores')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('branch_stocks', function(Blueprint $table)
		{
			$table->dropForeign('fk_BranchStock_Branch1');
			$table->dropForeign('fk_BranchStock_Store1');
		});
	}

}
