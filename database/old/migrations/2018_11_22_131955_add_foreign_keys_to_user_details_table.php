<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_details', function(Blueprint $table)
		{
			$table->foreign('branches_Id', 'fk_user_details_branches1')->references('Id')->on('branches')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('users_Id', 'fk_user_details_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_details', function(Blueprint $table)
		{
			$table->dropForeign('fk_user_details_branches1');
			$table->dropForeign('fk_user_details_users1');
		});
	}

}
