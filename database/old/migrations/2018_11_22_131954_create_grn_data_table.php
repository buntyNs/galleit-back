<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGrnDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grn_data', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->integer('ItemId',11)->index('FK_grn_data_items');
			$table->integer('Quntity',11);
			$table->integer('Available_Qty',11);//new
			$table->float('Price', 10, 0);
			$table->float('TotalPrice', 10, 0);
			$table->integer('Store_Id',10)->index('fk_GrnData_Store_idx');
			$table->integer('Grn_Id',11)->index('fk_GrnData_Grn1_idx');
			$table->string('Barcode',11);//new
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grn_data');
	}

}
