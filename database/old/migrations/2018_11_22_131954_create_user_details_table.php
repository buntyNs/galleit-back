<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_details', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->string('Address', 200);
			$table->integer('Contact');
			
			$table->integer('users_Id')->unsigned()->index('fk_user_details_users1_idx');
			$table->integer('branches_Id')->index('fk_user_details_branches1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_details');
	}

}
