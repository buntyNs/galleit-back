<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('Qty')->nullable();
			$table->integer('Store_Id')->index('fk_StoreItems_Store1_idx');
			$table->integer('Item_Id')->index('fk_StoreItems_Item1_idx');
		    $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_items');
	}

}
