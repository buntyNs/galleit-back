<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGrnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grns', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->integer('InvoiceId');
			$table->float('GrnTotal', 10, 0);
			$table->integer('Stock_Id');
			$table->integer('Status');
			$table->integer('Payment_Type_Id');//new
			$table->string('Payment_Due');//new
			//$table->integer('StockkeaperId')->unsigned()->index('fk_Grn_userDetails1_idx');//new
			$table->integer('user_Id')->unsigned()->index('fk_Grn_userDetails2_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grns');
	}

}
