<?php

namespace App\Http\Controllers;

use App\Api\Todo2;
use Illuminate\Http\Request;

class Todo2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Api\Todo2  $todo2
     * @return \Illuminate\Http\Response
     */
    public function show(Todo2 $todo2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Api\Todo2  $todo2
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo2 $todo2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Api\Todo2  $todo2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo2 $todo2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Api\Todo2  $todo2
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo2 $todo2)
    {
        //
    }
}
