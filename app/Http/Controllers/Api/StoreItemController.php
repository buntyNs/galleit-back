<?php

namespace App\Http\Controllers\Api;

use Validator;
use DB;
use App\model\StoreItem;
use App\model\StoreAdjustment;
use Illuminate\Http\Request;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;

class StoreItemController extends Controller
{
  
    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    public function index()
    {
        //
    }

    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        // $myModel = new StoreAdjustment;
        // return $myModel->getFillable();


        $validator = Validator::make($request->all(), [
            'user' => 'required',
        ]);
    
        DB::beginTransaction();
        try {
            
            if ($validator->fails()) {
                
                return (Validation::require());
            } 

                $item = explode(",",$id);
                $old =  StoreItem::where('Store_Id', $item[1])
                ->where('Item_Id', $item[0])
                ->select('Qty','Selling_Price')
                ->get();

                $store = StoreAdjustment::create([
                    'User_Id' => Request('user'),
                    'Store_Id' => $item[1],
                    'New_Qty' => $item[3],
                    'Old_Qty' => $old[0]->Qty,
                    'New_Selling_Price' => $item[2],
                    'Old_Selling_Price' => $old[0]->Selling_Price,
                    'Item_Id' => $item[0]
                   
                ]);
              

                if($store){

                $update =  StoreItem::where('Store_Id', $item[1])
                ->where('Item_Id', $item[0])
                ->update(['Qty' =>  $item[3], 'Selling_Price' =>  $item[2]]);

            if(  $update ){
                $item =StoreItem::join('items', 'items.id', '=', 'store_items.Item_Id')
                ->select('items.id','items.Name','items.Barcode','items.Units','store_items.Selling_Price','store_items.Qty')
                ->where('Store_Id',$item[1])
                ->get();
       
            }
        }
            DB::commit();
            return json_encode($item);
        
        }catch(Exception $e){
            DB::rollback();
            return($e);
        }





       }
    

   
    public function destroy(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            $update =  StoreItem::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            $item =StoreItem::join('items', 'items.id', '=', 'store_items.Item_Id')
            ->select('items.id','items.Name','items.Barcode','items.Units','store_items.Selling_Price','store_items.Qty')
            ->where('Store_Id',$request->store)
            ->where('store_items.Status',0)
            ->get();
    
    
            return json_encode($item);
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
