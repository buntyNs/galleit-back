<?php

namespace App\Http\Controllers\Api;
use Validator;
use DB;
use App\model\Transfer;
use App\model\TransferDetail;
use App\model\StoreItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;

class TransferController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api');
        
    }

    public function index()
    {
        //
    }

    public function getLastCode()
    {
        $grncode = Transfer::orderBy('created_at', 'desc')->first();
        if($grncode == ''){
            $comon = 1;
            $num_padded = sprintf("%05d", $comon);
           return 'TN'.$num_padded;
        }else{
            $gnId = $grncode->id + 1;
            $code =  sprintf("%05d", $gnId);
            return 'TN'.$code;
        }
    }

   
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        // 'User_Id','From_Id','To_Id','Status','Reason'
        // 'Qty','Transfer_Id','Item_Id'
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'from' => 'required',
            'to' => 'required',
            'note' => 'required',
            'code' => 'required',
            'data' => 'required',
        ]);
        DB::beginTransaction();
        try{
            if ($validator->fails()) {

                return (Validation::require());
            }

            $transfer = Transfer::create([
                'User_Id' => Request('user'),
                'From_Id' => Request('from'),
                'To_Id' => Request('to'),
                'Reason' => Request('note'),
                'Code' => Request('code'),
            ]);

            if($transfer){
                $transfer_id = $transfer->id;
                $transferArray = $request->data;
                $count =  count($transferArray);
                  // 'Qty','Transfer_Id','Item_Id'
                for($i = 0; $i < $count ; $i++){
                    $json = $transferArray[$i];
                    $data = TransferDetail::create([
                        'Item_Id' => $json ['id'],
                        'Qty' => $json['qty'],
                        'Transfer_Id' => $transfer_id,
                        'Name'=>$json['name']
                    ]);
                  
                    if($data){

                        $qty_old = StoreItem::where('Store_Id',Request('from'))
                        ->where('Item_Id', $json ['id'])
                        ->get();
                      
                        if($qty_old[0]->Qty >= $json['qty']){
                            $qty = $qty_old[0]->Qty - $json['qty'];
                          

                           $update =  StoreItem::where('Store_Id',Request('from'))
                           ->where('Item_Id',$json ['id'])
                           ->update(['Qty' => $qty]);

                           if($update){
                            $tostore = StoreItem::where('Store_Id',Request('to'))
                            ->where('Item_Id', $json ['id'])
                            ->get();

                            if(sizeof($tostore) != 0){
                                error_log('testing');
                                $qty2 = $tostore[0]->Qty + $json['qty'];
                                $update =  StoreItem::where('Store_Id',Request('to'))
                                ->where('Item_Id',$json ['id'])
                                ->update(['Qty' => $qty2]);
                            }else{

                                $store = StoreItem::create([
                                    'Store_Id' =>Request('to'),
                                    'Item_Id' => $json ['id'],
                                    'Selling_Price' => $json['price'],
                                    'Qty' => $json['qty'],
                                ]);

                            }
                           }

                        }else{
                            DB::rollback();
                            return (Validation::error("please check your stores quantity"));
                        }

                     
                    }


            }

        }
        DB::commit();
        return (Validation::success());
        }catch(Exception $e){
            DB::rollback();
            return (Validation::error($e));
        }




    }

    
    public function show($id)
    {
        $send = [];
        $note = DB::table('transfers')
        ->join('stores', 'stores.id', '=', 'transfers.From_Id')
        ->select('transfers.id','stores.Name','transfers.Code','transfers.Reason','transfers.created_at')
        ->where('transfers.Status',0)
        ->get();

        $note2 = DB::table('transfers')
        ->join('stores', 'stores.id', '=', 'transfers.To_Id')
        ->select('stores.Name')
        ->where('transfers.Status',0)
        ->get();
   
        for($i = 0 ; $i < sizeof($note) ; $i++){

            $obj = (object) [ //make an object in php
                'id' => $note[$i]->id,
                'code' =>$note[$i]->Code,
                'from' =>  $note[$i]->Name,
                'to' =>  $note2[$i]->Name,
                'reason' =>  $note[$i]->Reason,
                'date' =>  $note[$i]->created_at,
              
            ];
            array_push($send,$obj);
        }
      
        return json_encode($send);
    }

    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  Transfer::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(Transfer::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
