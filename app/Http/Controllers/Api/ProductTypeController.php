<?php

namespace App\Http\Controllers\Api;
use App\model\ItemType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;
use Validator;
use DB;

class ProductTypeController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api');
    }
   
    public function index()
    {
        return json_encode(ItemType::where('Status',0)->get());
    }

    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        // $myModel = new ItemType;
        // return $myModel->getFillable();
        $typeArray  = $request->type;
        $count =  count($typeArray);

        $validator = Validator::make($request->all(), [
            'type' => 'required',
        ]);

        if ($validator->fails()) {
                            
            return (Validation::require());
        } 
        DB::beginTransaction();
        try {
            for($i = 0 ; $i < $count ; $i++){
                $type = ItemType::create([
                    'Type' => $typeArray[$i] ,
                ]);
            }
            DB::commit();
            return (Validation::success());
        
        }catch (PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return ($splitName[0].'for'.$splitName[1]);
            }
            DB::rollback();
            return($e);

        }

    

    }

    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        
    }

  
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  ItemType::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(ItemType::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
