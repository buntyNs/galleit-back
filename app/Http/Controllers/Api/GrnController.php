<?php

namespace App\Http\Controllers\Api;
use DB;
use Validator;
use App\model\Grn;
use App\model\GrnData;
use App\model\StoreItem;
use App\model\LoanSupplier;
use App\model\RecordTransection;
use App\model\UserDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;
use App\Http\Controllers\Helper\Report;

class GrnController extends Controller
{
    public function __construct()
    {
       
        return $this->middleware('auth:api');
    }



public function getReport(Request $request){
    error_log($request);
    return (Report::getGrn($request->arr));
}



    public function index(Request $request)
    {
        $grnGet = Grn::join('suppliers','suppliers.id','grns.Supplier_Id')
        ->join('stores','stores.id','grns.Stock_Id')
        ->join('users','users.id','grns.User_Id')
        ->join('payment_types','payment_types.id','grns.Payment_Type_Id')
        ->select('grns.id','suppliers.Name as SName','grns.Grn_No'
        ,'grns.InvoiceId','grns.Grn_Total',
        'grns.Discount','stores.Name','users.name',
        'payment_types.Type')
        ->where('grns.Status',0)
        ->get();

        return json_encode($grnGet);
    
    }

    public function last(){
        $grncode = Grn::orderBy('created_at', 'desc')->first();
        if($grncode == ''){
            $comon = 1;
            $num_padded = sprintf("%05d", $comon);
           return 'GN'.$num_padded;
        }else{
            $gnId = $grncode->id + 1;
            $code =  sprintf("%05d", $gnId);
            return 'GN'.$code;
        }
    }

   
    public function create()
    {
      
    }

   
    public function store(Request $request)
    {
        error_log($request);
        $validator = Validator::make($request->all(), [
            'supplier' => 'required',
            'paymenttype' => 'required',
            'store' => 'required',
            'grnValue' => 'required',
            'discount' => 'required',
            'total' => 'required',
            'grn_number' => 'required',
            'invoiceNumber' => 'required',
            'user_id' => 'required',
        ]);
        DB::beginTransaction();
        try {
          
            if ($validator->fails()) {

                return(Validation::require());
            }

            // 'InvoiceId', 'Grn_Total', 'Stock_Id','Status','User_Id','Payment_Type_Id','Payment_Due','Grn_No','Grn_Value,'Supplier_Id
            $grn = Grn::create([
                'Grn_No' => Request('grn_number'),
                'InvoiceId' => Request('invoiceNumber'),
                'Grn_Total' => Request('total'),
                'Stock_Id' => Request('store'),
                'User_Id' => Request('user_id'),
                'Payment_Type_Id' => Request('paymenttype'),
                'Payment_Data' => Request('data'),
                'Grn_Value' => Request('grnValue'),
                'Supplier_Id' => Request('supplier'),
                'Discount' =>Request('discount')
            ]);
            error_log('test 1');

            if($grn){
                if(Request('paymenttype') == 4){
                    // 'Supplier_Id','Grn_Id','Amount','Due_Date','Status'
                    $loan=LoanSupplier::create([
                        'Supplier_Id'=> Request('supplier'),
                        'Grn_Id'=>$grn->id,
                        'Amount'=>Request('total'),
                        'Due_Amount'=>Request('total'),
                        'Due_Date'=>Request('dueDate'),
                     
                    ]);
                    error_log('this is test2');
                }

            }

            if($grn){
                // 'Item_Id','Qty','Available_Qty','Selling_Price','TotalPrice','Store_Id','Grn_Id','Barcode','Item_Price','Discount'
                $grn_id = $grn->id;
                $grnArray = $request->tableData;
                $count =  count($grnArray);
                error_log('test 2');
              
                for($i = 0; $i < $count ; $i++){
                    $json = $grnArray[$i];
                    $grn = GrnData::create([
                        'Item_Id' => $json ['id'],
                        'Qty' => $json['qty'],
                        'Available_Qty' => $json['qty'],
                        'Selling_Price' => $json['selling_price'],
                        'TotalPrice' => $json['price'],
                        'Grn_Id' => $grn_id,
                        'Barcode' => $json['code'],
                        'Item_Price' => $json['item_price'],
                        'Discount' =>$json['discount'],
                        'Store_Id' => Request('store'),
                    ]);

                

                    // 'Store_Id','Item_Id','Selling_Price','Qty'
                    if($grn){
                        error_log('test 3');
                    
                        $qty_old = StoreItem::where('Store_Id',Request('store'))
                        ->where('Item_Id', $json ['id'])
                        ->get();

                        if(sizeof($qty_old) != 0){

                            $qty = $qty_old[0]->Qty + $json['qty'];

                           $update =  StoreItem::where('Store_Id',Request('store'))
                            ->where('Item_Id',$json ['id'])
                            ->update(['Qty' => $qty, 'Selling_Price' =>  $json['selling_price']]);

                          
                           
                        }else{

                            $store = StoreItem::create([
                                'Store_Id' =>Request('store'),
                                'Item_Id' => $json ['id'],
                                'Selling_Price' => $json['selling_price'],
                                'Qty' => $json['qty'],
                            ]);
                          

                        }
                    }
        
                }


                $barnch_id = UserDetail::where('User_Id',Request('user_id'))
                ->select('Branch_Id')
                ->get();
                 $bid =  $barnch_id[0]->Branch_Id;

                 error_log($bid);
                RecordTransection::create([
                    'Code' => $grn_id,
                    'Type' => 'GRN',
                    'User' => Request('user_id'),
                    'Branch' => $bid
                   
                ]);


               
            }

            DB::commit();
            return(Validation::success());
        }catch(Exception $e){

            DB::rollback();
            return $e;
        }


















        // $validator = Validator::make($request->all(), [
        //     // 'Add_branch' => 'required',
        //     'InvoiceId' => 'required',
        //     'GrnTotal' => 'required',
        //     'StockId' => 'required',
        //     'StockkeaperId' => 'required',
        //     'user_Id' => 'required',
        //     'arr'=>'require'
        //     // 'tel' => 'required',

        // ]);

        // DB::beginTransaction();
        // try {
          
        //     if ($validator->fails()) {

        //         return response()->json(['error' => $validator->errors(),'req'=>$request], 401);
        //     }      

        //     $input = $request->all();
        //     $user = Grn::create([
        //         'InvoiceId'=>Request('InvoiceId'),
        //         'GrnTotal'=>Request('GrnTotal'),
        //         'StockId'=>Request('StockId'),
        //         'StockkeaperId'=>Request('StockkeaperId'),
        //         'user_Id'=>Request('user_Id'),
        //     ]);

          
   

        //     $lastid1= $user->id;
        //     // return $lastid1;

        //     $UserDetail=GrnData::create([
        //         'Address'=>Request('address'),
        //         'users_id'=>$lastid1,
        //         'Contact'=>Request('tel'),
        //         'branches_Id'=>Request('selectedBranch'),
        //     ]);
        //     // $lastid= $UserDetail->id;
            
        //     // return $lastid1;
        //     $UserAuthoriti=UserAuthoritie::create([
        //         'data'=>Request('arr'),
        //         'user_Id'=>$lastid1
               
        //     ]);

        //     DB::commit();
        //     return response()->json(['success' => $success], $this->successStatus);

        // } catch (\Exception $e) {

        //     DB::rollback();
        //     return $e;
            
       
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return "3";
        $input = $request->all();
        $k=json_decode($input['arr'],true);
        return $k[4];
        foreach($k as $d){
            return $d["Add_user"];
        }
    //    return $input['arr'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  Grn::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(Grn::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
