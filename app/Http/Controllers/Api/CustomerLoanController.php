<?php

namespace App\Http\Controllers\Api;
use DB;
use Validator;
use PDOException;
use App\model\LoanCustomer;
use App\model\LoanCustomerData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;

class CustomerLoanController extends Controller
{
    
    public function __construct()
    {
       
        return $this->middleware('auth:api');
    }

    public function index()
    {
        $loan = LoanCustomer::join('customers','customers.id','loan_customers.Customer_Id')
        ->join('invoices','invoices.id','loan_customers.Invoice_Id')
        ->join('branches','branches.id','invoices.Branch_Id')
        ->select('loan_customers.id','invoices.Invoice_NO','customers.Name','loan_customers.Amount','loan_customers.Due_Amount','loan_customers.Due_Date','loan_customers.created_at')
        ->where('loan_customers.Status',0)
        ->get();
        
        return json_encode($loan);

    }

    public function getLoanDetails($id){
        error_log($id);
        $loan =  LoanCustomer::where('loan_customers.Customer_Id',$id)
        ->select('loan_customers.Due_Amount','loan_customers.Due_Date')
        ->get();

        return json_encode($loan);
    }

   
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loan_id' => 'required',
            'payment' => 'required',
            'user_id' => 'required',
          
           
        ]);
        DB::beginTransaction();
        try {
            if ($validator->fails()) {
                
                return (Validation::require());
            } 
           
            // $myModel = new Test;
            // return $myModel->getFillable();
            
            $LOcode = LoanCustomerData::orderBy('created_at', 'desc')->first();
          
            $loanCode;
            if($LOcode == ''){
               
                $comon = 1;
                $num_padded = sprintf("%05d", $comon);
                $loanCode = 'LO'.$num_padded;

            }else{
               
                $gnId = $LOcode->id + 1;
                $code =  sprintf("%05d", $gnId);
                $loanCode = 'LO'.$code;
              
            }






            $customer = LoanCustomerData::create([
                'Loan_Id' => Request('loan_id'),
                'Payment' => Request('payment'),
                'User_Id' => Request('user_id'),
                'Bill_Id'=> $loanCode
            ]);

            if($customer){
                $amount_old = LoanCustomer::where('id',Request('loan_id'))
                ->select('Due_Amount')
                ->get();

                $amount_new = $amount_old[0]->Due_Amount -  Request('payment');

                $amount_old = LoanCustomer::where('id',Request('loan_id'))
                ->update(['Due_Amount' => $amount_new]);
                
            }else{
                DB::rollback();
            }

            // dd($customer);
         
            DB::commit();
            return (Validation::success());
        } catch (PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return($e);

        }
    }

   
    public function show($id)
    {
       $dtail =  LoanCustomerData::join('loan_customers','loan_customers.id','loan_customer_datas.Loan_Id')
                ->join('users','users.id','loan_customer_datas.User_Id')
                ->select('loan_customer_datas.id','loan_customer_datas.Payment','users.name','loan_customer_datas.Bill_Id','loan_customer_datas.created_at')
                ->where('loan_customer_datas.Loan_Id',$id)
                ->get();
                return json_encode($dtail);


        
    }

   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
          
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'contact' => 'required',
        ]);
        DB::beginTransaction();
        try {
          
            if ($validator->fails()) {

                return (Validation::require());
            }                                                   
            $company = Company::find($id);
            $company->CName = Request('name');
            $company->Address = Request('address');
            $company->Email = Request('email');
            $company->Contact = Request('contact');
            $company->save();

            DB::commit();
            return (Validation::success());

        }catch(PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return($e);
        }
    }

    
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  LoanCustomer::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(LoanCustomer::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
    }
}
