<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\RecordTransection;
use App\Http\Controllers\Helper\Validation;
use App\Http\Controllers\Helper\Report;
use Validator;
use DB;
use PDOException;

class RecordTransectionController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api');
    }
    

    public function index()
    {
      
        $recordGet = RecordTransection::join('branches','branches.id','record_transections.Branch')
        ->join('users','users.id','record_transections.User')
        ->select('record_transections.id','branches.Name as BName','record_transections.Type','record_transections.Code','users.name','record_transections.created_at')
        ->where('record_transections.Status',0)
        ->get();
        
        return json_encode($recordGet);
    }


    public function getRecord(Request $request){
        error_log($request);
        return (Report::getRecord($request->arr));
    }
}
