<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;
use App\User;
use App\model\UserDetail;
use App\model\UserAuthoritie;
use DB;
use Illuminate\Http\Request;
use Validator;
use PDOException;

class UserController extends Controller
{
    public $successStatus = 200;

    public function __construct()
    {
        return $this->middleware('auth:api');
        
    }

    public function index()
    {

        // return json_encode(User::all()) ;
        // $plucked = $collection->pluck('Name');

        // $plucked->all();

        // return $plucked;


        $users = DB::table('users')
        ->join('user_details', 'users.id', '=', 'user_details.User_Id')
        ->select('users.id','users.name','user_details.Address','users.email','user_details.Contact')
        ->where('Status',0)
        ->get();
      return json_encode($users) ;
    }

    public function create(Request $request)
    {   

        $output->writeln('hello');
        
    }
    

    public function store(Request $request)
    {
        error_log($request);
        $validator = Validator::make($request->all(), [
  
            'arr' => 'required',
            'email' => 'required',
            'name' => 'required',
            'password' => 'required',
            'selectedBranch' => 'required',
            'stores' => 'required',

        ]);

        DB::beginTransaction();
        try {
            // echo $request;
            if ($validator->fails()) {
                
                return (Validation::require());
            }    
                                                 
            $input = $request->all();
            $user = User::create([
                'Email'=>Request('email'),
                'Password'=>bcrypt($input['password']),
                'Name'=>Request('name'),
            ]);
          
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->name;
          
            $lastid1= $user->id;
          
            // error_log('come4');
         
            $UserDetail=UserDetail::create([
                'Address'=>Request('address'),
                'User_Id'=>$lastid1,
                'Contact'=>Request('tel'),
                'Branch_Id'=>Request('selectedBranch'),
            ]);
            // return response()->json(['success' => $lastid1], $this->successStatus);
            // // $lastid= $UserDetail->id;
          
            // // return $lastid1;
            $UserAuthoriti=UserAuthoritie::create([
                'Data'=>Request('arr'),
                'Store'=>Request( 'stores'),
                'User_Id'=>$lastid1,
               
            ]);
            
            DB::commit();
         
            return (Validation::success());

        } catch (PDOException $e) {

            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return (Validation::error($e));
            
       
        }
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return $id;
      
        $users = DB::table('users')
        ->join('user_details', 'users.id', '=', 'user_details.User_Id')
        ->where('users.id','=',$id)
        ->select('*')
        ->get();
      return json_encode($users) ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
        $validator = Validator::make($request->all(), [
            // 'Add_branch' => 'required',
            'arr' => 'required',
            'address' => 'required',
            'email' => 'required',
            'name' => 'required',
            'password' => 'required',
            'branch' => 'required',
            'tel' => 'required',
            'stores' => 'required',
           

        ]);

        DB::beginTransaction();
        try {
            // echo $request;
            if ($validator->fails()) {

                return response()->json(['error' => $validator->errors(),'req'=>$request], 401);
            }                                                   
            $input = $request->all();

            $user = User::find($id);
            $user->email=Request('email');
            $user->password=bcrypt($input['password']);
            $user->name=Request('name');
            $user->save();

            // $user = User::create([
            //     'email'=>Request('email'),
            //     'password'=>bcrypt($input['password']),
            //     'name'=>Request('name'),
            // ]);
          
            // $success['token'] = $user->createToken('MyApp')->accessToken;
            // $success['name'] = $user->name;

            // $lastid1= $user->id;
            // return $lastid1;
            $UserDetail = UserDetail::where('User_Id','=', $id)->first();
       
            $UserDetail->Address=Request('address');
            $UserDetail->Contact=Request('tel');
            
            $UserDetail->Branch_Id=Request('branch');
            
            $UserDetail->save();
            // try {
            // $UserDetail=UserDetail::create([
            //     'Address'=>Request('address'),
            //     'User_Id'=>$id,
            //     'Contact'=>Request('tel'),
            //     'Branch_Id'=>Request('branch'),
            // ]);
            // }catch(ValidationException $e)
            // {
            //     // Back to form with errors
            //     return response()->json(['error' => 'Unauthorised'], 401);;
            // }
            // $lastid= $UserDetail->id;
            
            // return $lastid1;
            $UserAuthoriti=UserAuthoritie::where('User_Id',$id)->first();
            $UserAuthoriti->Data=Request('arr');
            $UserAuthoriti->Store=Request('stores');
            $UserAuthoriti->save();

            // UserAuthoritie::create([
            //     'Data'=>Request('arr'),
            //     'User_Id'=>$id
               
            // ]);

            DB::commit();
            return response()->json(['success' => 'success'], $this->successStatus);

        } catch (\Exception $e) {

            DB::rollback();
            return $e;
            
       
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        DB::beginTransaction();
        try {
            $update =  User::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            $users = DB::table('users')
            ->join('user_details', 'users.id', '=', 'user_details.User_Id')
            ->select('users.id','users.name','user_details.Address','users.email','user_details.Contact')
            ->where('Status',0)
            ->get();
      return json_encode($users) ;
        } catch (Exception $e) {
            DB::rollback();
            return($e);
        }
        
    }
}
