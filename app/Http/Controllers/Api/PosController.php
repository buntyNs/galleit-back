<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use App\model\Item;
use App\model\ItemType;
use App\model\Branch;
use App\model\StoreItem;
use App\model\UserDetail;


class PosController extends Controller
{
   

    public function __construct()
    {
        return $this->middleware('auth:api');
    }


    public function index()
    {
        $item = DB::table('items')
        ->join('item_types', 'item_types.id','items.Type_Id')
        ->select('*')
        ->where('items.Status',0)
        ->get();
      return json_encode($item) ;
    }

  
    public function create()
    {
        //
    }

  
    public function store(Request $request)
    {
        //
    }

   
    public function show($id)
    {

        $barnch_id = UserDetail::where('User_Id',$id)
        ->select('Branch_Id')
        ->get();

         $bid =  $barnch_id[0]->Branch_Id;

         $store_id = Branch::where('id', $bid)
        ->select('DefaultStore_Id')
        ->get();
        $did =  $store_id[0]->DefaultStore_Id;

        $item = DB::table('items')
        ->join('item_types', 'item_types.id','items.Type_Id')
        ->join('store_items','store_items.Item_Id','items.id')
        ->where('store_items.Store_Id', $did)
        ->select('Barcode','Qty','Selling_Price','Type_Id','Units','items.id','items.Name')
        ->get();
      return json_encode($item) ;
    }

    public function showByType(Request $request)
    {
      
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'type' => 'required',
            
        ]);

        if ($validator->fails()) {
                
            return response()->json(['error' => $validator->errors(),'req'=>$request], 402);
        } 

     


        $barnch_id = UserDetail::where('User_Id',Request('user'))
        ->select('Branch_Id')
        ->get();

         $bid =  $barnch_id[0]->Branch_Id;

         $store_id = Branch::where('id', $bid)
        ->select('DefaultStore_Id')
        ->get();
    
        $did =  $store_id[0]->DefaultStore_Id;

        $item = DB::table('items')
        ->join('item_types', 'item_types.id','items.Type_Id')
        ->join('store_items','store_items.Item_Id','items.id')
        ->where('store_items.Store_Id', $did)
        ->where('items.Type_Id',Request('type'))
        ->select('Barcode','Qty','Selling_Price','Type_Id','Units','items.id','items.Name')
        ->get();
      return json_encode($item) ;
    }



    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        
    }
}
