<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Validation;
use Validator;
use DB;
use PDOException;
use App\model\StoreItem;
use App\model\Store;
use App\model\Item;
class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        return $this->middleware('auth:api');
    }
    public function index()
    {
        return json_encode(Store::where('Status',0)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        error_log($request);
        $validator = Validator::make($request->all(), [
            'name' => 'required',

        

        ]);

        DB::beginTransaction();
        try {
            if ($validator->fails()) {
                            
                return (Validation::require());
            } 

            Store::create([
                'Name' => Request('name'),
                'Address' => Request('address'),
                'Email' => Request('email'),
                'Contact' => Request('contact'),
             
            ]);

            DB::commit();
            return (Validation::success());
        } catch (PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return (Validation::error($e));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Store = Store::find($id);
        return json_encode( $Branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'contact' => 'required'

        ]);
        DB::beginTransaction();
        try {
            // echo $request;
            if ($validator->fails()) {
                            
                return (Validation::require());
            }                                                  
            // $input = $request->all();

            $Item = Store::find($id);
            $Item->Name=Request('name');
            $Item->Address=Request('address');
            $Item->Contact=Request('contact');     
            $Item->Email=Request('email');
            $Item->save();

            DB::commit();
            return (Validation::success());

        }catch (PDOException $e){
            $errorCode = $e->errorInfo[1];
            error_log($e->errorInfo[2]);
            if($errorCode == 1062){
                // houston, we have a duplicate entry problem
                $splitName = explode('for key', $e->errorInfo[2]);
                DB::rollback();
                return (Validation::duplicate($splitName[0].'for'.$splitName[1]));
            }
            DB::rollback();
            return (Validation::error($e));

        }
    }

   
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $update =  Store::where('id',$id)
            ->update(['Status' => 1]);
            DB::commit();
            return json_encode(Store::where('Status',0)->get());
        } catch (Exception $e) {
            DB::rollback();
            return (Validation::error($e));
        }
    }

    public function getItemById($id){

        $item =StoreItem::join('items', 'items.id', '=', 'store_items.Item_Id')
        ->select('items.id','items.Name','items.Barcode','items.Units','store_items.Selling_Price','store_items.Qty')
        ->where('Store_Id',$id)
        ->where('store_items.Status',0)
        ->get();


        return json_encode($item);



    }
}
