<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTransfersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transfers', function(Blueprint $table)
		{
			$table->foreign('From', 'fk_Transfer_Store1')->references('Id')->on('stores')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('To', 'fk_Transfer_Store2')->references('Id')->on('stores')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('userDetails_Id', 'fk_Transfer_userDetails1')->references('Id')->on('user_details')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transfers', function(Blueprint $table)
		{
			$table->dropForeign('fk_Transfer_Store1');
			$table->dropForeign('fk_Transfer_Store2');
			$table->dropForeign('fk_Transfer_userDetails1');
		});
	}

}
