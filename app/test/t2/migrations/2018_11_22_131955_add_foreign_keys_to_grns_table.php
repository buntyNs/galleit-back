<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGrnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('grns', function(Blueprint $table)
		{
			$table->foreign('StockkeaperId', 'fk_Grn_userDetails1')->references('Id')->on('user_details')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('userDetails_Id1', 'fk_Grn_userDetails2')->references('Id')->on('user_details')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('grns', function(Blueprint $table)
		{
			$table->dropForeign('fk_Grn_userDetails1');
			$table->dropForeign('fk_Grn_userDetails2');
		});
	}

}
