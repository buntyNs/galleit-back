<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGrnDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grn_data', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->integer('ItemId');
			$table->integer('Quntity');
			$table->float('Price', 10, 0);
			$table->float('TotalPrice', 10, 0);
			$table->integer('Grn_Id')->index('fk_GrnData_Grn1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grn_data');
	}

}
