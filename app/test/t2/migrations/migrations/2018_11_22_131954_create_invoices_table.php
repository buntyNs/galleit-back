<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table)
		{
			$table->integer('Id', true);
		
			$table->float('Discount', 10, 0)->nullable();
			$table->float('TotalPrice', 10, 0)->nullable();
			$table->string('Status', 45)->nullable();
			$table->integer('userDetails_Id')->index('fk_Invoice_userDetails1_idx');
			$table->integer('Branch_Id')->index('fk_Invoice_Branch1_idx');
			$table->integer('PaymentType_Id')->index('fk_Invoice_PaymentType1_idx');
			$table->integer('Customer_Id')->index('fk_Invoice_Customer1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}
