<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStoreItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('store_items', function(Blueprint $table)
		{
			$table->foreign('Item_Id', 'fk_StoreItems_Item1')->references('Id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Store_Id', 'fk_StoreItems_Store1')->references('Id')->on('stores')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('store_items', function(Blueprint $table)
		{
			$table->dropForeign('fk_StoreItems_Item1');
			$table->dropForeign('fk_StoreItems_Store1');
		});
	}

}
