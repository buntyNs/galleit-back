<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLoanCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('loan_customers', function(Blueprint $table)
		{
			$table->foreign('invoices_Id', 'fk_loan_customers_invoices1')->references('Id')->on('invoices')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('loan_customers', function(Blueprint $table)
		{
			$table->dropForeign('fk_loan_customers_invoices1');
		});
	}

}
