<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGrnDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('grn_data', function(Blueprint $table)
		{
			$table->foreign('Grn_Id', 'fk_GrnData_Grn1')->references('Id')->on('grns')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('grn_data', function(Blueprint $table)
		{
			$table->dropForeign('fk_GrnData_Grn1');
		});
	}

}
