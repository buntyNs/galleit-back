<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGrnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grns', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->integer('InvoiceId');
	
			$table->float('GrnTotal', 10, 0);
			$table->integer('StockId');
			$table->integer('Status');
			$table->integer('StockkeaperId')->index('fk_Grn_userDetails1_idx');
			$table->integer('userDetails_Id1')->index('fk_Grn_userDetails2_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grns');
	}

}
