<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoanCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('loan_customers', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->float('Amount', 10, 0)->nullable();
			$table->dateTime('Date')->nullable();
			$table->float('PaidAmount', 10, 0)->nullable();
			$table->string('Type', 45)->nullable();
			$table->integer('Status')->nullable();
			$table->integer('invoices_Id')->index('fk_loan_customers_invoices1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('loan_customers');
	}

}
