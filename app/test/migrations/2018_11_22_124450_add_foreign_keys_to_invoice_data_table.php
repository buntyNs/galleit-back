<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInvoiceDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoice_data', function(Blueprint $table)
		{
			$table->foreign('Invoice_Id', 'fk_InvoiceData_Invoice')->references('Id')->on('invoices')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Item_Id', 'fk_InvoiceData_Item1')->references('Id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoice_data', function(Blueprint $table)
		{
			$table->dropForeign('fk_InvoiceData_Invoice');
			$table->dropForeign('fk_InvoiceData_Item1');
		});
	}

}
