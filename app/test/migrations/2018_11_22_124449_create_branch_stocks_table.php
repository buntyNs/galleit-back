<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBranchStocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('branch_stocks', function(Blueprint $table)
		{
			$table->integer('Id', true);
			$table->integer('BranchId')->nullable();
			$table->integer('StoreId')->nullable();
			$table->string('ItemId', 45)->nullable();
			$table->integer('Branch_Id')->index('fk_BranchStock_Branch1_idx');
			$table->integer('Store_Id')->index('fk_BranchStock_Store1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('branch_stocks');
	}

}
