<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTransferDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transfer_details', function(Blueprint $table)
		{
			$table->foreign('Item_Id', 'fk_TransferDetail_Item1')->references('Id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Transfer_Id', 'fk_TransferDetail_Transfer1')->references('Id')->on('transfers')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transfer_details', function(Blueprint $table)
		{
			$table->dropForeign('fk_TransferDetail_Item1');
			$table->dropForeign('fk_TransferDetail_Transfer1');
		});
	}

}
