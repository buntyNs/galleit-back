<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			$table->foreign('Branch_Id', 'fk_Invoice_Branch1')->references('Id')->on('branches')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Customer_Id', 'fk_Invoice_Customer1')->references('Id')->on('customers')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('PaymentType_Id', 'fk_Invoice_PaymentType1')->references('Id')->on('paymenttypes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('userDetails_Id', 'fk_Invoice_userDetails1')->references('Id')->on('user_details')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			$table->dropForeign('fk_Invoice_Branch1');
			$table->dropForeign('fk_Invoice_Customer1');
			$table->dropForeign('fk_Invoice_PaymentType1');
			$table->dropForeign('fk_Invoice_userDetails1');
		});
	}

}
