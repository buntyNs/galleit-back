<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = [
        'Name','Address','Contact','Status','Email'
      ];
}
