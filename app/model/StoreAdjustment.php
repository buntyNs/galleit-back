<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class StoreAdjustment extends Model
{
    protected $fillable = [
        'User_Id','Store_Id','Item_Id','Old_Qty','New_Qty','Old_Selling_Price','New_Selling_Price','Status'
      ];
}
