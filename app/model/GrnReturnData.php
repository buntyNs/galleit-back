<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class GrnReturnData extends Model
{
    protected $fillable = ['Note_Id', 'Item_Id' ,'Qty' , 'Total'];
}
