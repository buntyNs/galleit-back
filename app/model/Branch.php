<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{

    protected $fillable = [
        'Address', 'Name','Contact','Mid','DefaultStore_Id','Email','Status'
    ];
}
