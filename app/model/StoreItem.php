<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class StoreItem extends Model
{
    protected $fillable = [
        'Store_Id','Item_Id','Selling_Price','Qty','Status'
      ];
}
