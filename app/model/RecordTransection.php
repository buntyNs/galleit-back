<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class RecordTransection extends Model
{
    protected $fillable = [
        'Code', 'Type','User','Branch','Status'
    ];
}
