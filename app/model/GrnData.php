<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class GrnData extends Model
{
    protected $fillable = [
        'Item_Id','Qty','Available_Qty','Selling_Price','TotalPrice','Store_Id','Grn_Id','Barcode','Item_Price','Discount'
    ];
}
