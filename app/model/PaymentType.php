<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    protected $fillable = [
        'Type','Status'
      ];
}
