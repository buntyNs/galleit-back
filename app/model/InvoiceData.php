<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InvoiceData extends Model
{
    protected $fillable = [
        'Qty','Free_Qty','Discount','Selling_Price','Total_Price','Invoice_Id','Item_Id',
     ];
}
