<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class CustomerReturn extends Model
{
    protected $fillable = ['Invoice_Id' ,'Reason' ,'Refund', 'Item_Id','Qty','Status'];
}
