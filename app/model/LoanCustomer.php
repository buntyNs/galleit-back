<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class LoanCustomer extends Model
{
    protected $fillable = [
       'Customer_Id','Invoice_Id','Amount','Due_Amount','Due_Date','Status'
     ];
}
