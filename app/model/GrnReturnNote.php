<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class GrnReturnNote extends Model
{
    protected $fillable = ['Grn_Id', 'User_Id' ,'Supplier_Id','Item_Id','Store_Id','Qty','Refound','Reason','Status'];
}
