<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $fillable = [
        'User_Id','Code','From_Id','To_Id','Reason','Status','created_at'
    ];
}
