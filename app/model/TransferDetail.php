<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class TransferDetail extends Model
{
    protected $fillable = [
        'Qty','Transfer_Id','Item_Id','Name'
    ];
}
