<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class LoanCustomerData extends Model
{
    protected $fillable = [
        'Loan_Id','Payment','User_Id','Customer_Id','Bill_Id'
      ];
}
