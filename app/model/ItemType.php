<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    protected $fillable = [
        'Type','Status'
     ];
}
