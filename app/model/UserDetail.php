<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = [
       'Address','Contact','User_Id','Branch_Id',
    ];
}
