<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Grn extends Model
{

    protected $fillable = [
        'InvoiceId', 'Grn_Total', 'Stock_Id','Status','User_Id','Payment_Type_Id','Payment_Data','Grn_No','Grn_Value','Supplier_Id','Discount'
    ];
}
