<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['Name','Email','Contact','Address','Nic'];
}
