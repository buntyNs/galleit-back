<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class CustomerI extends Model
{
    protected $table = 'customers';
    protected $fillable = ['Name','Email','Contact','Address','Nic'];
}
